package com.hellospring.demo.mapper;

import com.hellospring.demo.exception.ResourceNotFoundException;
import com.hellospring.demo.model.Dosen;
import com.hellospring.demo.model.Mahasiswa;
import com.hellospring.demo.model.MataKuliah;
import com.hellospring.demo.model.dto.ListDosenDto;
import com.hellospring.demo.repository.DosenRepository;
import com.hellospring.demo.repository.MahasiswaRepository;
import com.hellospring.demo.repository.MataKuliahRepository;
import com.hellospring.demo.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PostDosenMapper {
    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;


    public Dosen dosenPostToEntity(ListDosenDto listDosenDto) {
        Dosen dosen =modelMapperUtil.modelMapperUtil().map(listDosenDto, Dosen.class);
        MataKuliah mataKuliah = mataKuliahRepository.findById(listDosenDto.getMataKuliah_id()).
                orElseThrow(()-> new ResourceNotFoundException("Mata Kuliah Not Found"));


        dosen.setName_dosen(listDosenDto.getName_dosen());
        dosen.setAddress_dosen(listDosenDto.getAddress_dosen());
        dosen.setMataKuliah(mataKuliah);

        return dosenRepository.save(dosen);
    }
}
