package com.hellospring.demo.mapper;

import com.hellospring.demo.exception.ResourceNotFoundException;
import com.hellospring.demo.model.Dosen;
import com.hellospring.demo.model.Mahasiswa;
import com.hellospring.demo.model.dto.ListMahasiswaDto;
import com.hellospring.demo.repository.DosenRepository;
import com.hellospring.demo.repository.MahasiswaRepository;
import com.hellospring.demo.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostMahasiswaMapper {
    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private DosenRepository dosenRepository;

    public Mahasiswa mahasiswaPostToEntity(ListMahasiswaDto listMahasiswaDto){
        Mahasiswa mahasiswa = modelMapperUtil.modelMapperUtil().map(listMahasiswaDto,Mahasiswa.class);
        Dosen dosen = dosenRepository.findById(listMahasiswaDto.getDosenid()).
                orElseThrow(()-> new ResourceNotFoundException("Dosen not Found"));
        mahasiswa.setName_mahasiswa(listMahasiswaDto.getName_mahasiswa());
        mahasiswa.setAddress_mahasiswa(listMahasiswaDto.getAddress_mahasiswa());
        mahasiswa.setDosen(dosen);

        return mahasiswaRepository.save(mahasiswa);
    }
}
