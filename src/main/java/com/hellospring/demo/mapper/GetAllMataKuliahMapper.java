package com.hellospring.demo.mapper;

import com.hellospring.demo.model.MataKuliah;
import com.hellospring.demo.model.dto.MataKuliahDto;
import com.hellospring.demo.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetAllMataKuliahMapper {
    @Autowired
    private ModelMapperUtil modelMapperutil;

    public List<MataKuliahDto> mapperMataKuliahDto(List<MataKuliah> mataKuliahs){
        List<MataKuliahDto> mataKuliahDtos = new ArrayList<>();
        for (MataKuliah data : mataKuliahs){
            MataKuliahDto mataKuliahDto = modelMapperutil.modelMapperUtil().map(data, MataKuliahDto.class);
            mataKuliahDto.setId_mataKuliah(data.getId_mataKuliah());
            mataKuliahDto.setName_course(data.getName_course());


            mataKuliahDtos.add(mataKuliahDto);
        }
        return mataKuliahDtos;
    }
}
