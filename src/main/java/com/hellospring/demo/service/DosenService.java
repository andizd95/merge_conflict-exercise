package com.hellospring.demo.service;

import com.hellospring.demo.model.Dosen;
import com.hellospring.demo.model.dto.ListDosenDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

public interface DosenService {
    List<ListDosenDto> getDataDosen();
    ResponseEntity<Object> postDosen(@RequestBody ListDosenDto listDosenDto);

}
