package com.hellospring.demo.service;

import com.hellospring.demo.model.dto.ListMahasiswaDto;
import com.hellospring.demo.model.dto.MataKuliahDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface MataKuliahService {
    List<MataKuliahDto> getAllMataKuliah();
    ResponseEntity<Object> postMataKuliah(@RequestBody MataKuliahDto mataKuliahDto);

}
