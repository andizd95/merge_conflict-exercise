package com.hellospring.demo.service.impl;

import com.hellospring.demo.mapper.GetAllMahasiswaMapper;
import com.hellospring.demo.mapper.PostMahasiswaMapper;
import com.hellospring.demo.model.Mahasiswa;
import com.hellospring.demo.model.dto.ListMahasiswaDto;
import com.hellospring.demo.repository.MahasiswaRepository;
import com.hellospring.demo.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {

    @Autowired
    private PostMahasiswaMapper postMahasiswaMapper;

    @Autowired
    private GetAllMahasiswaMapper getAllMahasiswaMapper;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public List<ListMahasiswaDto> getAllMahasiswa() {
        List<Mahasiswa> mahasiswas = mahasiswaRepository.findAll();
        List<ListMahasiswaDto> listMahasiswaDtos = getAllMahasiswaMapper.mapperMahasiswaDto(mahasiswas);

        return listMahasiswaDtos;
    }

    @Override
    public ResponseEntity<Object> postMahasiswa(ListMahasiswaDto listMahasiswaDto) {
        Mahasiswa mahasiswa = postMahasiswaMapper.mahasiswaPostToEntity(listMahasiswaDto);

        return new ResponseEntity<>(mahasiswa, HttpStatus.OK);
    }
}
