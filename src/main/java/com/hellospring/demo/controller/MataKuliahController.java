package com.hellospring.demo.controller;

import com.hellospring.demo.model.Dosen;
import com.hellospring.demo.model.MataKuliah;
import com.hellospring.demo.model.dto.MataKuliahDto;
import com.hellospring.demo.repository.MataKuliahRepository;
import com.hellospring.demo.service.impl.MataKuliahServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class MataKuliahController {
    @Autowired
    private MataKuliahServiceImpl mataKuliahService;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @GetMapping("/mata")
    public List<MataKuliahDto> getMataKuliah(){

        return mataKuliahService.getAllMataKuliah();
    }


    @PostMapping("/mata")
    public ResponseEntity<Object> postMataKuliah(@RequestBody MataKuliahDto mataKuliahDto){
        return mataKuliahService.postMataKuliah(mataKuliahDto);
    }



}
