package com.hellospring.demo.controller;

import com.hellospring.demo.model.dto.ListMahasiswaDto;
import com.hellospring.demo.service.impl.MahasiswaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class MahasiswaController {
    @Autowired
    private MahasiswaServiceImpl mahasiswaService;

    @GetMapping("/mahasiswa")
    public List<ListMahasiswaDto> getAllMahasiswa(){

        return mahasiswaService.getAllMahasiswa();
    }

    @PostMapping("/mahasiswa")
    public ResponseEntity<Object> postMahasiswa(@RequestBody ListMahasiswaDto listMahasiswaDto){
        return mahasiswaService.postMahasiswa(listMahasiswaDto);
    }



}
