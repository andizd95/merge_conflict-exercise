package com.hellospring.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "mahasiswa")
@Entity
public class Mahasiswa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_mahasiswa")
    private Long id_mahasiswa;
    @Column(name = "name_mahasiswa")
    private String name_mahasiswa;
    @Column(name = "address_mahasiswa")
    private String address_mahasiswa;

//    @JsonIgnore
//    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dosen_id",referencedColumnName = "id")
    private Dosen dosen;
}
