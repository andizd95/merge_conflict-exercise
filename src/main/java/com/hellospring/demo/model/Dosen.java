package com.hellospring.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "dosen")
public class Dosen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name_dosen")
    private String name_dosen;
    @Column(name = "address_dosen")
    private String address_dosen;

//    @JsonIgnore
    @JsonBackReference
    @OneToMany(mappedBy = "dosen")
    private List<Mahasiswa> mahasiswa;

//    @JsonIgnore
//    @JsonBackReference
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "matakuliah_id")
    private MataKuliah mataKuliah;


}
