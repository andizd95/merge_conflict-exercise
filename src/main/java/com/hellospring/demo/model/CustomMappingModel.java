package com.hellospring.demo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@SqlResultSetMapping(name = "QueryNativePakeJoin", entities = {
        @EntityResult( entityClass = CustomMappingModel.class, fields = {
                @FieldResult(name = "id", column = "id"),
                @FieldResult(name = "nameMahasiswa", column = "name_mahasiswa"),
                @FieldResult(name = "nameDosen", column = "name_dosen"),
                @FieldResult(name = "nameCourse", column = "name_course"),
        })
})
public class CustomMappingModel {
    @Id
    private String id;
    private String nameMahasiswa;
    private String nameDosen;
    private String nameCourse;
}
