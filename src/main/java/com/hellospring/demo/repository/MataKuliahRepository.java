package com.hellospring.demo.repository;


import com.hellospring.demo.model.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MataKuliahRepository extends JpaRepository<MataKuliah,Long> {

}
